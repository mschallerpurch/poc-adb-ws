Proof of Concept on Ad-Blocked Websockets
=========================================

Initial configuration
---------------------

### Node
[Node](https://nodejs.org/en/) needs to be installed on the host system to run this POC.

### Alias
The objective here is to add a new alias for `localhost`, with a name that is blacklisted from the AdBlock extensions. To do so, edit the file `/etc/hosts` and add the line:
```
127.0.0.1       ads.poc
```
The changes are immediately applied, no need to reboot the system.
The alias `ads.poc` was chosen because it contains the string `ads*`, which is usually immediately blocked.

### Dependency installation
Inside the project directory, run:
```
npm install
```

Start the server
----------------

To start the server, run:
```
node server.js
```

Experiment
----------

Open the page `localhost:8080/index.html` on your browser. This page will try to download a script located at `ads.poc:8080/http.js`. Since `ads.poc` is just an alias for the host machine, the script that is being retrieved is the one located under the `static/` folder.

* If an AdBlock extension is **disabled**, there should be a message on the console that says: `HTTP: It worked!` (because that's what the script does);
* If an AdBlock extension is **enabled**, there should be no message displayed on the console, and an error that says: `GET http://ads.poc:8080/script.js net::ERR_BLOCKED_BY_CLIENT`.

The page will also open a WebSocket connection, to the same domain: `ads.poc:8080`, on the `ws://` protocol. This connection, if it is successful (displaying `WebSocket connection established.` on the server console), will then feed the client with the content of the JavaScript file located on `static/websocket.js`. The client will then evaluate it.

* If an AdBlock extension is **disabled**, there should be a message on the console that says: `WebSocket: It worked!` (because that's what the script does);
* If an AdBlock extension is **enabled**, the **exact same** behavior is observed: the extension couldn't block the connection.