const express = require('express');
const fs = require('fs');
const app = express();
const server = require('http').createServer(app);
const WebSocketServer = require('ws').Server;
const wss = new WebSocketServer({ server: server });

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.use(express.static('static'));

wss.on('connection', ws => {
    console.log('WebSocket connection established.');

    fs.readFile('./static/websocket.js', 'utf-8', (err, content) => {
        ws.send(content);
    });
});

server.listen(8080, () => {
    console.log('Server started. Visit http://localhost:8080');
});